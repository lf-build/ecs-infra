#!/bin/bash

scriptdir=`dirname $0`
scriptname=`basename $0`
cd $scriptdir

display_usage() {
  echo -e "\nUsage:\n${scriptname} <project-name> <project-env> <infra-module> <cmd> \n"
  echo -e "Avaliable Infra Modules: \n`ls -1 infra` \n"
  echo -e "Avaliable Commands: \ninit\nplan\napply\ndestroy\nbash\n"
}

project_name=$1
shift
project_env=$1
shift
project_module=$1
shift

[[ -z ${project_name} || -z ${project_env} || -z ${project_module} ]] && display_usage && exit 1


export PROJECT_NAME=$project_name
export PROJECT_ENV=$project_env
export TF_VAR_project_name=$PROJECT_NAME
export TF_VAR_project_env=$PROJECT_ENV
export TF_VAR_project_module=$project_module

proj=${TF_VAR_project_name}-${TF_VAR_project_env}
export PROJECT=${proj}
export AWS_PROFILE=${PROJECT}
export TF_VAR_aws_profile=${AWS_PROFILE}

tmpdir=.provision
mkdir -p $tmpdir/$proj/$project_module/modules

[[ -d ./modules ]] && rsync -az --delete modules/ $tmpdir/$proj/$project_module/modules/
cat infra/$project_module/*.tf > $tmpdir/$proj/$project_module/main.tf

echo "" > $tmpdir/$proj/$project_module/main.vars
[[ -f vars/main.vars ]] && cat vars/main.vars >> $tmpdir/$proj/$project_module/main.vars
[[ -f vars/$project_name/main.vars ]] && cat vars/$project_name/main.vars >> $tmpdir/$proj/$project_module/main.vars
[[ -f vars/$proj/main.vars ]] && cat vars/$proj/main.vars >> $tmpdir/$proj/$project_module/main.vars
[[ -f vars/$proj/$project_module/main.vars ]] && cat vars/$proj/$project_module/main.vars >> $tmpdir/$proj/$project_module/main.vars
[[ -f vars/local.vars ]] && cat vars/local.vars >> $tmpdir/$proj/$project_module/main.vars

echo "" > $tmpdir/$proj/$project_module/terraform.tfvars
[[ -f vars/terraform.tfvars ]] && cat vars/terraform.tfvars >> $tmpdir/$proj/$project_module/terraform.tfvars
[[ -f vars/$project_name/terraform.tfvars ]] && cat vars/$project_name/terraform.tfvars >> $tmpdir/$proj/$project_module/terraform.tfvars
[[ -f vars/$proj/terraform.tfvars ]] && cat vars/$proj/terraform.tfvars >> $tmpdir/$proj/$project_module/terraform.tfvars
[[ -f vars/$proj/$project_module/terraform.tfvars ]] && cat vars/$proj/$project_module/terraform.tfvars >> $tmpdir/$proj/$project_module/terraform.tfvars
[[ -f vars/local.tfvars ]] && cat vars/local.tfvars >> $tmpdir/$proj/$project_module/terraform.tfvars

[[ -f $tmpdir/$proj/$project_module/main.vars ]] && source $tmpdir/$proj/$project_module/main.vars

[[ -z $TERRAFORM_PATH ]] && TERRAFORM_PATH=terraform

cmd=$1
shift

case "${cmd}" in

init)
  cd $tmpdir/$proj/$project_module

  [[ -d ./.terraform ]] && rm -rf .terraform
  $TERRAFORM_PATH remote config \
    -backend=s3 \
    -backend-config="bucket=${TF_VAR_terraform_state_s3_bucket}" \
    -backend-config="key=${proj}/${project_module}.tfstate" \
    -backend-config="region=${TF_VAR_aws_region}" \
    -backend-config="profile=${TF_VAR_aws_profile}"
  [[ $? == 0 ]] && $TERRAFORM_PATH get -update
  ;;

plan)
  cd $tmpdir/$proj/$project_module

  $TERRAFORM_PATH remote pull
  $TERRAFORM_PATH plan
  ;;

apply | provision)
  cd $tmpdir/$proj/$project_module

  $TERRAFORM_PATH remote pull
  $TERRAFORM_PATH apply
  ;;

destroy)
  cd $tmpdir/$proj/$project_module

  $TERRAFORM_PATH remote pull
  $TERRAFORM_PATH destroy
  ;;

bash)
  cd $tmpdir/$proj/$project_module

  PS1="$tmpdir/$proj/$project_module :) " bash
  ;;

out)
  cd $tmpdir/$proj/$project_module
  $TERRAFORM_PATH output $@
  ;;

tf)
  cd $tmpdir/$proj/$project_module
  $TERRAFORM_PATH $@
  ;;

*)
  display_usage
  ;;

esac

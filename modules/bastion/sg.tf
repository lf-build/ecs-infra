
resource "aws_security_group" "bastion" {
  description = "Controls direct access to bastion instance"
  vpc_id      = "${var.vpc_id}"
  name        = "asg-${var.project_name}-${var.project_env}-bastion"

  ingress {
    protocol  = "tcp"
    from_port = 22
    to_port   = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

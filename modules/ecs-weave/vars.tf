variable "aws_region" {
  description = "AWS Region."
}

variable "az_count" {
  description = "Number of AZs to cover in a given AWS region"
}

variable "project_name" {
  description = "The project name."
}

variable "project_env" {
  description = "The project env."
}

variable "key_name" {
  description = "Name of AWS key pair"
}

variable "instance_type" {
  default     = "t2.small"
  description = "AWS instance type"
}

variable "vpc_id" {
  description = "The VPC ID"
}

variable "vpc_public_subnet_ids" {
  description = "The VPC subnet ids"
  type = "list"
}

variable "vpc_private_subnet_ids" {
  description = "The VPC subnet ids"
  type = "list"
}

variable "asg_min" {
  description = "Min numbers of servers in ASG"
  default     = "1"
}

variable "asg_max" {
  description = "Max numbers of servers in ASG"
  default     = "2"
}

variable "asg_desired" {
  description = "Desired numbers of servers in ASG"
  default     = "1"
}

variable "asg_memr_high_thresold" {
  description = "MemoryReservation Upper Thresold"
  default     = "70"
}

variable "asg_memr_low_thresold" {
  description = "MemoryReservation Lower Thresold"
  default     = "20"
}

variable "alrm_ecs_memr_thresold" {
  description = "ECS MemoryReservation Thresold"
  default     = "80"
}

variable "ecs_weave_ami_name" {
  description = "Latest ECS WEAVE AMI Name"
  default     = "Weaveworks ECS Image *"
}

variable "log_retention_in_days" {
  description = "cloudwatch log retention in days"
}

variable "user_data_type" {}
variable "http_enabled" {}
variable "https_enabled" {}
variable "lbsg_http_enabled" {}
variable "lbsg_https_enabled" {}
variable "ssl_cert_arn" {}
variable "weavescope_ssl_cert_arn" {}
variable "services_ssl_cert_arn" {}
variable "bankportal_ssl_cert_arn" {}
variable "backoffice_ssl_cert_arn" {}
variable "ui_ssl_cert_arn" {}
variable "app_ssl_cert_arn" {}
variable "orbit_ssl_cert_arn" {}
variable "verify_ssl_cert_arn" {}
variable "static_ssl_cert_arn" {}
variable "whitelisted_cidr_list" { type = "list" }
variable "ecs_ansible_s3_region" {}
variable "ecs_ansible_s3_bucket" {}
variable "ecs_ansible_s3_prefix" {}
variable "base_amis" { type = "map" }
variable "root_disk_size" {}
variable "fs_s3_bucket" {}
variable "fs_s3_prefix" {}
variable "ui_maint_tg_arn" {}

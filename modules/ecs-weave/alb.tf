## ALB

resource "aws_alb" "ops" {
  name            = "alb-${var.project_name}-${var.project_env}-ops"
  internal        = false
  subnets         = ["${var.vpc_public_subnet_ids}"]
  security_groups = ["${aws_security_group.ops_lb.id}"]
  idle_timeout    = 60
  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_alb_target_group" "weavescope" {
  name     = "tg-${var.project_name}-${var.project_env}-weavescope"
  port     = 4040
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"
  stickiness {
    type = "lb_cookie"
  }
  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_alb_listener" "weavescope_http" {
  count             = "${var.http_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "4040"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.weavescope.id}"
    type             = "forward"
  }
}

resource "aws_alb_listener" "weavescope_https" {
  count             = "${var.https_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "4443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2015-05"
  certificate_arn   = "${coalesce(var.weavescope_ssl_cert_arn, var.ssl_cert_arn)}"

  default_action {
    target_group_arn = "${aws_alb_target_group.weavescope.id}"
    type             = "forward"
  }
}

resource "aws_alb_target_group" "services" {
  name     = "tg-${var.project_name}-${var.project_env}-services"
  port     = 9090
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"

  health_check {
    path = "/ping"
    port = 8080
  }

  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_alb_listener" "services_http" {
  count             = "${var.http_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "9090"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.services.id}"
    type             = "forward"
  }
}

resource "aws_alb_listener" "services_https" {
  count             = "${var.https_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "9443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2015-05"
  certificate_arn   = "${coalesce(var.services_ssl_cert_arn, var.ssl_cert_arn)}"

  default_action {
    target_group_arn = "${aws_alb_target_group.services.id}"
    type             = "forward"
  }
}

resource "aws_alb_target_group" "backoffice" {
  name     = "tg-${var.project_name}-${var.project_env}-backoffice"
  port     = 9002
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"

  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_alb_listener" "backoffice_http" {
  count             = "${var.http_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "8080"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.backoffice.id}"
    type             = "forward"
  }
}

resource "aws_alb_listener" "backoffice_https" {
  count             = "${var.https_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "8443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2015-05"
  certificate_arn   = "${coalesce(var.backoffice_ssl_cert_arn, var.ssl_cert_arn)}"

  default_action {
    target_group_arn = "${aws_alb_target_group.backoffice.id}"
    type             = "forward"
  }
}

resource "aws_alb_target_group" "bankportal" {
  name     = "tg-${var.project_name}-${var.project_env}-bankportal"
  port     = 9002
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"

  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_alb_listener" "bankportal_http" {
  count             = "${var.http_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "7070"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.bankportal.id}"
    type             = "forward"
  }
}

resource "aws_alb_listener" "bankportal_https" {
  count             = "${var.https_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "7443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2015-05"
  certificate_arn   = "${coalesce(var.bankportal_ssl_cert_arn, var.ssl_cert_arn)}"

  default_action {
    target_group_arn = "${aws_alb_target_group.bankportal.id}"
    type             = "forward"
  }
}

resource "aws_alb_target_group" "static" {
  name     = "tg-${var.project_name}-${var.project_env}-static"
  port     = 8080
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"

  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_alb_listener" "static_http" {
  count             = "${var.http_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "6060"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.static.id}"
    type             = "forward"
  }
}

resource "aws_alb_listener" "static_https" {
  count             = "${var.https_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "6443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2015-05"
  certificate_arn   = "${coalesce(var.static_ssl_cert_arn, var.ssl_cert_arn)}"

  default_action {
    target_group_arn = "${aws_alb_target_group.static.id}"
    type             = "forward"
  }
}

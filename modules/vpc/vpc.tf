### Network

data "aws_availability_zones" "available" {}

resource "aws_vpc" "main" {
  cidr_block = "${var.vpc_cidr}"
  enable_dns_support = true
  enable_dns_hostnames = true

  tags {
    Name = "vpc-${var.project_name}-${var.project_env}"
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_subnet" "public" {
  count             = "${var.az_count}"
  cidr_block        = "${cidrsubnet(aws_vpc.main.cidr_block, 8, count.index)}"
  availability_zone = "${data.aws_availability_zones.available.names[count.index]}"
  vpc_id            = "${aws_vpc.main.id}"

  tags {
    Name = "subnet-${var.project_name}-${var.project_env}-public"
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_subnet" "private" {
  count             = "${var.az_count}"
  cidr_block        = "${cidrsubnet(aws_vpc.main.cidr_block, 8, 10 + count.index)}"
  availability_zone = "${data.aws_availability_zones.available.names[count.index]}"
  vpc_id            = "${aws_vpc.main.id}"

  tags {
    Name = "subnet-${var.project_name}-${var.project_env}-private"
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.main.id}"

  tags {
    Name = "igw-${var.project_name}-${var.project_env}"
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_eip" "nat" {
  vpc   = true
}

resource "aws_nat_gateway" "nat" {
  allocation_id  = "${aws_eip.nat.id}"
  subnet_id      = "${aws_subnet.public.0.id}"
  depends_on     = ["aws_internet_gateway.gw"]
}

resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.main.id}"

  tags {
    Name = "rt-${var.project_name}-${var.project_env}-public"
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_route_table" "private" {
  vpc_id = "${aws_vpc.main.id}"

  tags {
    Name = "rt-${var.project_name}-${var.project_env}-private"
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_route_table_association" "public" {
  count          = "${var.az_count}"
  subnet_id      = "${element(aws_subnet.public.*.id, count.index)}"
  route_table_id = "${aws_route_table.public.id}"
}

resource "aws_route_table_association" "private" {
  count          = "${var.az_count}"
  subnet_id      = "${element(aws_subnet.private.*.id, count.index)}"
  route_table_id = "${aws_route_table.private.id}"
}

resource "aws_route" "igw" {
  route_table_id = "${aws_route_table.public.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = "${aws_internet_gateway.gw.id}"
}

resource "aws_route" "nat" {
  route_table_id = "${aws_route_table.private.id}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id = "${aws_nat_gateway.nat.id}"
}

data "aws_vpc_peering_connection" "pc" {
  count = "${var.vpc_peering_route_enabled}"
  id = "${var.vpc_peering_id}"
}

resource "aws_route" "vpc_peering_public" {
  count = "${var.vpc_peering_route_enabled}"
  route_table_id = "${aws_route_table.public.id}"
  destination_cidr_block = "${element(data.aws_vpc_peering_connection.pc.*.cidr_block, count.index)}"
  vpc_peering_connection_id = "${element(data.aws_vpc_peering_connection.pc.*.id, count.index)}"
}

resource "aws_route" "vpc_peering_private" {
  count = "${var.vpc_peering_route_enabled}"
  route_table_id = "${aws_route_table.private.id}"
  destination_cidr_block = "${element(data.aws_vpc_peering_connection.pc.*.cidr_block, count.index)}"
  vpc_peering_connection_id = "${element(data.aws_vpc_peering_connection.pc.*.id, count.index)}"
}


instance_type = "m4.4xlarge"
asg_min = 1
asg_desired = 1
asg_max = 1
az_count = 3

root_disk_size = 50
alrm_ecs_memr_thresold = 85
asg_memr_high_thresold = 85

user_data_type = "custom1"

http_enabled = true
https_enabled = true

#aws_cert_arn = "arn:aws:acm:us-east-1:993978665313:certificate/cea1ea13-3301-4100-aaae-b65ff7197300"
#original_root_cert_arn = "arn:aws:acm:us-east-1:993978665313:certificate/7881c26e-fad2-4c60-a588-14ca84720b57"
#portal_cert_arn = "arn:aws:acm:us-east-1:993978665313:certificate/39ac7404-9bb2-44af-b13d-5975eef7b4a9"
#api_cert_arn = "arn:aws:acm:us-east-1:993978665313:certificate/829ff044-8a22-4fdc-ae42-7538cf9d440f"
#root_cert_arn = "arn:aws:acm:us-east-1:993978665313:certificate/03d06577-4360-4014-9b93-c47bbc86eedb"

ssl_cert_arn = ""
weavescope_ssl_cert_arn = "arn:aws:acm:us-east-1:993978665313:certificate/39ac7404-9bb2-44af-b13d-5975eef7b4a9"
services_ssl_cert_arn = "arn:aws:acm:us-east-1:993978665313:certificate/829ff044-8a22-4fdc-ae42-7538cf9d440f"
#services_ssl_cert_arn = "arn:aws:acm:us-east-1:993978665313:certificate/39ac7404-9bb2-44af-b13d-5975eef7b4a9"
bankportal_ssl_cert_arn = "arn:aws:acm:us-east-1:993978665313:certificate/39ac7404-9bb2-44af-b13d-5975eef7b4a9"
backoffice_ssl_cert_arn = "arn:aws:acm:us-east-1:993978665313:certificate/39ac7404-9bb2-44af-b13d-5975eef7b4a9"
ui_ssl_cert_arn = "arn:aws:acm:us-east-1:993978665313:certificate/39ac7404-9bb2-44af-b13d-5975eef7b4a9"
#app_ssl_cert_arn = "arn:aws:acm:us-east-1:993978665313:certificate/829ff044-8a22-4fdc-ae42-7538cf9d440f"
app_ssl_cert_arn = "arn:aws:acm:us-east-1:993978665313:certificate/39ac7404-9bb2-44af-b13d-5975eef7b4a9"
verify_ssl_cert_arn = "arn:aws:acm:us-east-1:993978665313:certificate/39ac7404-9bb2-44af-b13d-5975eef7b4a9"

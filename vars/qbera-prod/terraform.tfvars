
instance_type = "m4.4xlarge"
asg_min = 1
asg_desired = 1
asg_max = 1

root_disk_size = 120
alrm_ecs_memr_thresold = 90
asg_memr_high_thresold = 85

user_data_type = "custom1"

http_enabled = false
https_enabled = true
lbsg_http_enabled = false
lbsg_https_enabled = true
#ssl_cert_arn = "arn:aws:acm:ap-southeast-1:993978665313:certificate/d40e2186-8e93-43d3-88c1-7a877f468347"
ssl_cert_arn = "arn:aws:acm:ap-southeast-1:993978665313:certificate/352865b8-cedd-4494-919f-0ee773ab7945" 

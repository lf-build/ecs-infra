
variable "aws_region" {}
variable "aws_profile" {}

variable "project_name" {}
variable "project_env" {}

variable "vpc_cidr" {}
variable "az_count" {}

variable "vpc_peering_route_enabled" {
  default = false
}

variable "vpc_peering_id" {
  default = ""
}

provider "aws" {
  region = "${var.aws_region}"
  profile = "${var.aws_profile}"
}

module "vpc" {
  source = "./modules/vpc"

  project_name = "${var.project_name}"
  project_env = "${var.project_env}"
  aws_region = "${var.aws_region}"
  vpc_cidr = "${var.vpc_cidr}"
  aws_profile = "${var.aws_profile}"
  az_count = "${var.az_count}"

  vpc_peering_route_enabled = "${var.vpc_peering_route_enabled}"
  vpc_peering_id = "${var.vpc_peering_id}"
}

output "vpc_id" { value = "${module.vpc.vpc_id}" }
output "vpc_private_subnet_ids" { value = "${module.vpc.vpc_private_subnet_ids}" }
output "vpc_public_subnet_ids" { value = "${module.vpc.vpc_public_subnet_ids}" }
output "vpc_nat_ip" { value = "${module.vpc.vpc_nat_ip}" }

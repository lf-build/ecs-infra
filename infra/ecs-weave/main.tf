
variable "aws_region" {}
variable "az_count" {}
variable "aws_profile" {}
variable "project_name" {}
variable "project_env" {}
variable "terraform_state_s3_bucket" {}
variable "terraform_state_s3_key_vpc" {}
variable "key_name" {}
variable "instance_type" {}
variable "asg_min" {}
variable "asg_max" {}
variable "asg_desired" {}
variable "asg_memr_high_thresold" { default = "70" }
variable "asg_memr_low_thresold" { default = "20" }
variable "alrm_ecs_memr_thresold" { default = "80" }
variable "log_retention_in_days" { default = 0 }
variable "user_data_type" { default = "default" }
variable "http_enabled" { default = 1 }
variable "https_enabled" { default = 0 }
variable "lbsg_http_enabled" { default = 1 }
variable "lbsg_https_enabled" { default = 0 }
variable "ssl_cert_arn" { default = "" }
variable "weavescope_ssl_cert_arn" { default = "" }
variable "services_ssl_cert_arn" { default = "" }
variable "bankportal_ssl_cert_arn" { default = "" }
variable "backoffice_ssl_cert_arn" { default = "" }
variable "ui_ssl_cert_arn" { default = "" }
variable "app_ssl_cert_arn" { default = "" }
variable "orbit_ssl_cert_arn" { default = "" }
variable "verify_ssl_cert_arn" { default = "" }
variable "static_ssl_cert_arn" { default = "" }
variable "whitelisted_cidr_list" { type = "list" }
variable "ecs_ansible_s3_region" { default = "" }
variable "ecs_ansible_s3_bucket" { default = "" }
variable "ecs_ansible_s3_prefix" { default = "ecs-ansible" }
variable "base_amis" { type = "map" }
variable "root_disk_size" { default = 50 }
variable "fs_s3_bucket" { default = "" }
variable "fs_s3_prefix" { default = "" }
variable "ui_maint_tg_arn" { default = "" }

provider "aws" {
  region = "${var.aws_region}"
  profile = "${var.aws_profile}"
}

data "terraform_remote_state" "vpc" {
    backend = "s3"
    config {
        bucket = "${var.terraform_state_s3_bucket}"
        key = "${var.terraform_state_s3_key_vpc}"
        region = "${var.aws_region}"
        profile = "${var.aws_profile}"
    }
}

module "ecs" {
  source = "./modules/ecs-weave"

  aws_region = "${var.aws_region}"
  az_count = "${var.az_count}"

  project_name = "${var.project_name}"
  project_env = "${var.project_env}"

  key_name = "${var.key_name}"
  instance_type = "${var.instance_type}"
  asg_min = "${var.asg_min}"
  asg_max = "${var.asg_max}"
  asg_desired = "${var.asg_desired}"
  asg_memr_high_thresold = "${var.asg_memr_high_thresold}"
  asg_memr_low_thresold = "${var.asg_memr_low_thresold}"
  alrm_ecs_memr_thresold = "${var.alrm_ecs_memr_thresold}"

  vpc_id = "${data.terraform_remote_state.vpc.vpc_id}"
  vpc_public_subnet_ids = "${data.terraform_remote_state.vpc.vpc_public_subnet_ids}"
  vpc_private_subnet_ids = "${data.terraform_remote_state.vpc.vpc_private_subnet_ids}"

  log_retention_in_days = "${var.log_retention_in_days}"
  user_data_type = "${var.user_data_type}"

  http_enabled = "${var.http_enabled}"
  https_enabled = "${var.https_enabled}"
  lbsg_http_enabled = "${var.lbsg_http_enabled}"
  lbsg_https_enabled = "${var.lbsg_https_enabled}"
  ssl_cert_arn = "${var.ssl_cert_arn}"
  weavescope_ssl_cert_arn = "${var.weavescope_ssl_cert_arn}"
  services_ssl_cert_arn = "${var.services_ssl_cert_arn}"
  bankportal_ssl_cert_arn = "${var.bankportal_ssl_cert_arn}"
  backoffice_ssl_cert_arn = "${var.backoffice_ssl_cert_arn}"
  static_ssl_cert_arn = "${var.static_ssl_cert_arn}"
  ui_ssl_cert_arn = "${var.ui_ssl_cert_arn}"
  app_ssl_cert_arn = "${var.app_ssl_cert_arn}"
  orbit_ssl_cert_arn = "${var.orbit_ssl_cert_arn}"
  verify_ssl_cert_arn = "${var.verify_ssl_cert_arn}"

  whitelisted_cidr_list = ["${var.whitelisted_cidr_list}"]
  ecs_ansible_s3_region = "${var.ecs_ansible_s3_region}"
  ecs_ansible_s3_bucket = "${var.ecs_ansible_s3_bucket}"
  ecs_ansible_s3_prefix = "${var.ecs_ansible_s3_prefix}"

  base_amis = "${var.base_amis}"
  root_disk_size = "${var.root_disk_size}"
  fs_s3_bucket = "${var.fs_s3_bucket}"
  fs_s3_prefix = "${var.fs_s3_prefix}"

  ui_maint_tg_arn = "${var.ui_maint_tg_arn}"
}

output "ecs_cluster_name" { value = "${module.ecs.ecs_cluster_name}" }
output "ecs_service_role_name" { value = "${module.ecs.ecs_service_role_name}" }
output "ecs_service_role_arn" { value = "${module.ecs.ecs_service_role_arn}" }
output "ecs_service_autoscaling_role_arn" { value = "${module.ecs.ecs_service_autoscaling_role_arn}" }

output "alb_id" { value = "${module.ecs.ops_alb_id}" }
output "alb_hostname" { value = "${module.ecs.ops_alb_hostname}" }
output "alb_tg_services" { value = "${module.ecs.alb_tg_services}" }
output "alb_tg_backoffice" { value = "${module.ecs.alb_tg_backoffice}" }
output "alb_tg_bankportal" { value = "${module.ecs.alb_tg_bankportal}" }
output "alb_tg_static" { value = "${module.ecs.alb_tg_static}" }

output "alb_ui_id" { value = "${module.ecs.alb_ui_id}" }
output "alb_ui_hostname" { value = "${module.ecs.alb_ui_hostname}" }
output "alb_tg_ui_proxy" { value = "${module.ecs.alb_tg_ui_proxy}" }
output "alb_tg_ui_maint" { value = "${module.ecs.alb_tg_ui_maint}" }
output "alb_tg_ui" { value = "${module.ecs.alb_tg_ui}" }

output "alb_app_id" { value = "${module.ecs.alb_app_id}" }
output "alb_app_hostname" { value = "${module.ecs.alb_app_hostname}" }
output "alb_tg_app" { value = "${module.ecs.alb_tg_app}" }

output "alb_verify_id" { value = "${module.ecs.alb_verify_id}" }
output "alb_verify_hostname" { value = "${module.ecs.alb_verify_hostname}" }
output "alb_tg_verify" { value = "${module.ecs.alb_tg_verify}" }

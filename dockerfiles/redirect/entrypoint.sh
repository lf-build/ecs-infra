#!/bin/sh

# Other variables for flexible config
[[ -z $REDIRECT_SCHEME ]] && REDIRECT_SCHEME=https
[[ -z $REDIRECT_HOST ]] && REDIRECT_HOST="\$host"
REDIRECT_HOST_PORT=${REDIRECT_HOST}
[[ ! -z $REDIRECT_PORT ]] && REDIRECT_HOST_PORT="${REDIRECT_HOST}:${REDIRECT_PORT}"

# important env variables
[[ -z $PORT ]] && export PORT=80
[[ -z $REDIRECT_URL ]] && export REDIRECT_URL="${REDIRECT_SCHEME}://$REDIRECT_HOST_PORT\$request_uri"
[[ -z $HTTP_CODE ]] && export HTTP_CODE=307

envsubst '$PORT $HTTP_CODE $REDIRECT_URL' < /nginx.conf.template > /etc/nginx/conf.d/default.conf

[[ ! -z $DEBUG ]] && echo $PORT $HTTP_CODE $REDIRECT_URL
[[ ! -z $DEBUG ]] && cat /etc/nginx/conf.d/default.conf

exec nginx -g 'daemon off;'


VER := $(shell cat VERSION)
DIST=dist
NAME=ecs-infra-$(VER)
TARGET=$(DIST)/$(NAME)
TARGET1=$(DIST)/$(NAME)-vars

all: pkg

pkg:
	rm -rf $(TARGET)/
	mkdir -p $(TARGET)/
	cp -r infra $(TARGET)/
	cp -r modules $(TARGET)/
	cp -r services $(TARGET)/
	cp ecs $(TARGET)/
	cp tf $(TARGET)/
	cd $(DIST) && tar -zcf $(NAME).tar.gz $(NAME)/

pkgvars:
	rm -rf $(TARGET1)/
	mkdir -p $(TARGET1)/
	cp -r vars $(TARGET1)/
	cd $(DIST) && tar -zcf $(NAME)-vars.tar.gz $(NAME)-vars/

clean:
	rm -rf $(DIST)/

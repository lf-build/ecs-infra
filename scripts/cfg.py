#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
import sys
import urllib2
import os.path


endpoint = os.getenv('CONFIGURATION_ENDPOINT', '')
token = os.getenv('CONFIGURATION_TOKEN', '')
dryrun = os.getenv('DRY_RUN', os.getenv('DRYRUN', ''))

def main(service, rulespath):
    if not os.path.isfile(rulespath):
        return
    rules = None
    with open(rulespath) as rules_file:
        rules = json.load(rules_file)
    context = {}
    request = urllib2.Request(endpoint + '/' + service, headers={"Authorization" : "Bearer " + token})
    try:
        response = urllib2.urlopen(request)
        if response.getcode() == 200:
            data = json.load(response)
            #print json.dumps(data, indent=2)
            pathtokens = []
            context['updated'] = False
            context['rules'] = rules
            data_iterator(data, '', pathtokens, context)
            update_cfg(service, data, context)
        else:
            print service, 'Invalid Code: ', response.getcode()
    except urllib2.HTTPError, e:
        print service, 'Error Code: ', e.getcode()

def data_iterator(ref, key, pathtokens, context):
    if key == '':
        value = ref
    else:
        pathtokens.append(key)
        value = ref[key]
    if isinstance(value, dict):
        for key in value:
            data_iterator(value, key, pathtokens, context)
    elif isinstance(value, list):
        for i, item in enumerate(value):
            data_iterator(value, i, pathtokens, context)
    elif isinstance(value, basestring):
        update_value(ref, key, value, pathtokens, context)
    if len(pathtokens) > 0:
        pathtokens.pop()

# Do changes to the payload based on some rules.
def update_value(payload, key, value, pathtokens, context):
    if not value.startswith('http'):
        return
    if value.startswith('http://api.creditexchange.com'):
        return
    rules = context['rules']
    if rules is None:
        return
    rule = get_rule(pathtokens, rules)
    if "name" not in rule:
        rule["name"] = pathtokens[-1]
    if "value" in rule:
        rvalue = get_val(rule, rule["value"], value)
    if "baseurl" in rule:
        baseurl = get_val(rule, rule["baseurl"], value)
        i = value.index('/', value.index('://') + 3)
        rvalue = baseurl + value[i:]
    if value == rvalue:
        return
    context['updated'] = True
    payload[key] = rvalue
    print '   ', '/'.join(pathtokens), ":", value, ">>>", rvalue

def get_val(rule, value, defaultValue):
    if value.startswith("env:"):
        return os.getenv(value[4:])
    if value.startswith("env-tmpl:"):
        value = os.getenv(value[9:])
        return value.format(**rule)
    if value.startswith("ignore:"):
        return defaultValue
    return value

def get_rule(pathtokens, rules):
    rule_default_key = "/".join(pathtokens[:-1 or None]) + "/*"
    rule_key = "/".join(pathtokens)
    the_default_rule = None
    the_rule = None
    rule = {}
    if rule_default_key in rules:
        rule = dict(rules[rule_default_key].items())
    if rule_key in rules:
        rule = dict(rule.items() + rules[rule_key].items())
    return rule

def update_cfg(service, data, context):
    if not context['updated']:
        print service, ': No Change! Ignoring!'
        return
    print service, ': Updating!'
    url = endpoint + '/' + service
    print service, ': POST', url
    #print json.dumps(data, indent=2)
    if len(dryrun) > 0:
        return
    print service, ': POST Call Not Implemented!'
    return
    req = urllib2.Request(url, json.dumps(data), {'Content-Type': 'application/json', "Authorization" : "Bearer " + token})
    try:
        response = urllib2.urlopen(req)
        if response.getcode() == 200:
            print response.read()
        else:
            print service, 'Invalid Code: ', response.getcode()
    except urllib2.HTTPError, e:
        print service, 'Error Code: ', e.getcode()

if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2])
